import { Item, GildedRose } from '../app/gilded-rose';

describe('Snapshot', () => {

   it('should match snapshot', () => {
	const items = [
    		new Item("+5 Dexterity Vest", 10, 20), //
    		new Item("Aged Brie", 2, 0), //
    		new Item("Elixir of the Mongoose", 5, 7), //
    		new Item("Sulfuras, Hand of Ragnaros", 0, 80), //
    		new Item("Sulfuras, Hand of Ragnaros", -1, 80),
    		new Item("Backstage passes to a TAFKAL80ETC concert", 15, 20),
    		new Item("Backstage passes to a TAFKAL80ETC concert", 10, 49),
    		new Item("Backstage passes to a TAFKAL80ETC concert", 5, 49),
    		// this conjured item does not work properly yet
    		new Item("Conjured Mana Cake", 3, 6)
	];


	const gildedRose = new GildedRose(items);
	const days = 2;
	let output = '';
	for (let i = 0; i < days; i++) {
    		output += "-------- day " + i + " --------\n";
    		output += "name, sellIn, quality\n";
    		items.forEach(element => {
        		output += element.name + ' ' + element.sellIn + ' ' + element.quality + "\n"
    		});
    		gildedRose.updateQuality();
	}
		expect(output).toMatchSnapshot()

	});
});

describe('Gilded Rose', function () {

    it('should foo', function() {
        const gildedRose = new GildedRose([ new Item('foo', 0, 0) ]);
        const items = gildedRose.updateQuality();
        expect(items[0].name).toEqual('foo');
    });

		it('should degrade the quality twice faster when the sell in date has passed', () => {
			const gildedRose = new GildedRose([ new Item('foo', 0, 2)]);
			const items = gildedRose.updateQuality()
			expect(items[0].quality).toEqual(0)
		})

		it('should not allow the quality to become negative', () => {
			const gildedRose = new GildedRose([ new Item('foo', 0, 0)]);
			const items = gildedRose.updateQuality()
			expect(items[0].quality).toBeGreaterThanOrEqual(0)
		})

		it('should increase the quality by one unit each day before the sell in date', () => {
			const gildedRose = new GildedRose([ new Item('Aged Brie', 1, 0)]);
			const items = gildedRose.updateQuality()
			expect(items[0].quality).toEqual(1)
		})

		it('should increase the quality by two units each day after the sell in date', () => {
			const gildedRose = new GildedRose([ new Item('Aged Brie', 0, 0)]);
			const items = gildedRose.updateQuality()
			expect(items[0].quality).toEqual(2)
		})

		it('should not allow to increase a quality over 50 units', () => {
			const gildedRose = new GildedRose([ new Item('Aged Brie', 0, 49)]);
			const items = gildedRose.updateQuality()
			expect(items[0].quality).toBeLessThanOrEqual(50)
		})

		it('should not modify neither sell in date neither the quality with Sulfuras item', () => {
			const ORIGINAL_SELL_IN = 0
			const ORIGINAL_QUALITY = 0
			const gildedRose = new GildedRose([ new Item('Sulfuras, Hand of Ragnaros', ORIGINAL_SELL_IN, ORIGINAL_QUALITY)])
			const items = gildedRose.updateQuality()
			expect(items[0].quality).toBeGreaterThanOrEqual(ORIGINAL_QUALITY)
			expect(items[0].sellIn).toEqual(ORIGINAL_SELL_IN)
		})

		it('should increase the Backstage quality by one unit each day passes before 10 days of sell in', () => {
			const gildedRose = new GildedRose([ new Item('Backstage passes to a TAFKAL80ETC concert', 11, 2)])
			const items = gildedRose.updateQuality()
			expect(items[0].quality).toEqual(3)
		})

		it('should increase the Backstage quality by two units each day passes after 10 days but before 5 days of sell in', () => {
			const gildedRose = new GildedRose([ new Item('Backstage passes to a TAFKAL80ETC concert', 10, 2)])
			const items = gildedRose.updateQuality()
			expect(items[0].quality).toEqual(4)
		})

		it('should increase the Backstage quality by two units each day passes after 5 days but before 0 days of sell in', () => {
			const gildedRose = new GildedRose([ new Item('Backstage passes to a TAFKAL80ETC concert', 5, 2)])
			const items = gildedRose.updateQuality()
			expect(items[0].quality).toEqual(5)
		})

		it('should decrease the Backstage quality to when the sell in date has passed', () => {
			const gildedRose = new GildedRose([ new Item('Backstage passes to a TAFKAL80ETC concert', 0, 2)])
			const items = gildedRose.updateQuality()
			expect(items[0].quality).toEqual(0)
		})
});
